var MainApp;

MainApp = (function() {
  function MainApp() {
    return ['ionic', 'ionic.service.core'];
  }

  return MainApp;

})();

angular.module('app', new MainApp());

var Configuration;

Configuration = (function() {
  function Configuration($rootScope, $ionicPlatform, $ionicLoading) {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  }

  return Configuration;

})();

angular.module('app').run(['$rootScope', '$ionicPlatform', '$ionicLoading', Configuration]);

var Categories, CategoryDetail, CategoryItemDetail, Main, Settings;

Main = (function() {
  function Main($scope, $ionicModal, $timeout, $state) {
    $scope.menuItems = [
      {
        text: "Categories",
        action: function() {
          return $state.go('app.categories');
        }
      }, {
        text: "Call Waiter",
        action: function() {
          return console.log("Call Waiter");
        }
      }, {
        text: "Request Check",
        action: function() {
          return console.log("Request Check");
        }
      }, {
        text: "About DabbletMenu",
        action: function() {
          return console.log("About DabbletMenu");
        }
      }, {
        text: "Settings",
        action: function() {
          return $state.go('conf.settings');
        }
      }
    ];
  }

  return Main;

})();

Settings = (function() {
  function Settings($scope, $ionicPopup, LocalDB) {
    $scope.viewTitle = "Settings";
    $scope.viewId = "settings-view";
    $scope.settings = JSON.parse(localStorage.getItem('settings'));
    $scope.save = function() {
      return $ionicPopup.confirm({
        title: 'Kaydet',
        template: 'Ayarları değiştirmek istediğinizden emin misiniz ?',
        cancelText: 'Hayır',
        okText: 'Evet'
      }).then(function(res) {
        if (res) {
          return localStorage.setItem('settings', JSON.stringify($scope.settings));
        }
      });
    };
  }

  return Settings;

})();

Categories = (function() {
  function Categories($scope, $state) {
    $scope.viewTitle = "Categories";
    $scope.viewId = "categoryMain";
    $scope.categories = [
      {
        title: "Appetizers",
        id: 1,
        image: "img/thumb/appetizers.jpg",
        like: 21
      }, {
        title: "Salads",
        id: 2,
        image: "img/thumb/Chopped-Salad_550x310.jpg",
        like: 43
      }, {
        title: "Meats",
        id: 3,
        image: "img/thumb/5491610d-3014-41e4-a4b1-72e1c0a8641a.jpg",
        like: 23
      }, {
        title: "Seafood",
        id: 4,
        image: "img/thumb/healthy-seafood.jpg",
        like: 25
      }, {
        title: "Fish",
        id: 5,
        image: "img/thumb/gida02.jpg",
        like: 54
      }, {
        title: "Chef Special",
        id: 6,
        image: "img/thumb/da-tandoor-victoria-01.jpg",
        like: 74
      }, {
        title: "Side Orders",
        id: 7,
        image: "img/thumb/4371111_ec17e07834.jpg",
        like: 61
      }, {
        title: "Drinks",
        id: 8,
        image: "img/thumb/ProductsSoftDrinks-604-st.jpg",
        like: 22
      }
    ];
    $scope.goCategoryDetail = function(id) {
      return $state.go('app.categoryDetail', {
        id: id
      });
    };
  }

  return Categories;

})();

CategoryDetail = (function() {
  function CategoryDetail($scope, AppModals) {
    $scope.category = {
      id: 1,
      title: "Appetizers",
      image: "img/thumb/appetizers.jpg",
      isImageless: false,
      products: [
        {
          title: 'Chicken, Beef Or Cheese Mini Patties',
          price: "3.75",
          image: 'img/thumb/28358_l.jpg'
        }, {
          title: 'Fried Cassava',
          price: "3.75",
          desc: "With green peppers, red peppers and onion",
          image: 'img/thumb/YucaFritamini.jpg'
        }, {
          title: 'Sampler Platter (Small)',
          price: "12",
          desc: "With combination of fried meat, green plantains and cassava, with fried cheese add $2.00",
          image: 'img/thumb/appetizer-platter.jpg'
        }, {
          title: 'Grilled Steak',
          price: "10.50",
          desc: 'Sautee with green peppers, red peppers and onion',
          image: 'img/thumb/250514100857-beef-steak.jpg'
        }, {
          title: 'Seafood Combination',
          price: "30.50",
          desc: 'With lobster, shrimps, calamari, octopus & mussels',
          image: 'img/thumb/55011.jpg'
        }, {
          title: 'Macaroni Shells',
          price: "30.50",
          desc: 'With lobster, shrimps, calamari, octopus & mussels',
          image: 'img/thumb/tumblr_mzaovujDaf1qg5ngpo1_500.jpg'
        }, {
          title: 'Macaroni Shells',
          price: "30.50",
          desc: 'With lobster, shrimps, calamari, octopus & mussels',
          image: 'img/thumb/250514100857-beef-steak.jpg'
        }, {
          title: 'Grilled Chicken Salad',
          price: "2500",
          image: 'img/thumb/appetizer-platter.jpg'
        }
      ]
    };
    $scope.openDetail = function(id) {
      return AppModals.categoryItemDetail({
        id: id
      });
    };
  }

  return CategoryDetail;

})();

CategoryItemDetail = (function() {
  function CategoryItemDetail($scope, AppModals, parameters) {
    $scope.item = {
      title: 'Macaroni Shells',
      price: "30.50",
      desc: 'With lobster, shrimps, calamari, octopus & mussels',
      longDesc: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going yo use a passage of Lorem Ipsum.",
      images: ['img/thumb/tumblr_mzaovujDaf1qg5ngpo1_500.jpg', 'img/thumb/250514100857-beef-steak.jpg', 'img/thumb/appetizer-platter.jpg', 'img/thumb/55011.jpg']
    };
  }

  return CategoryItemDetail;

})();

angular.module('app').controller('mainController', ['$scope', '$ionicModal', '$timeout', '$state', Main]).controller('settingsController', ['$scope', '$ionicPopup', 'LocalDB', Settings]).controller('categoriesController', ['$scope', '$state', Categories]).controller('categoryDetailController', ['$scope', 'AppModals', CategoryDetail]).controller('categoryItemDetailController', ['$scope', 'AppModals', 'parameters', CategoryItemDetail]);

var Constants, Remote;

Constants = (function() {
  function Constants($state, $ionicPopup) {
    var apiRoot, settings;
    settings = JSON.parse(localStorage.getItem('settings'));
    apiRoot = "127.0.0.1:8000";
    if (!settings) {
      return $ionicPopup.alert({
        title: 'Server bilgisi girilmemiş',
        template: 'Server bilgisi girebilmek için ayarlar bölümüne yönlendirileceksiniz.',
        okText: 'Tamam'
      }).then(function() {
        return $state.go('conf.settings');
      });
    }
    return {
      urls: {
        login: "http://" + apiRoot + "/login",
        api: "http://" + apiRoot + "/api?user=" + settings.username
      }
    };
  }

  return Constants;

})();

Remote = (function() {
  function Remote($rootScope, $http, $q, $log, $timeout, $ionicPopup, $ionicLoading, Constants) {
    var errorCode, errorHandler, getDeferred, postDeferred;
    errorCode = function(code, url) {
      var error;
      error = {
        0: "Bağlantınızda sorunlar bulunmaktadır. Lütfen daha sonra tekrar deneyiniz.",
        400: "Hatalı İstek <br>" + code + " <br> " + url,
        401: "Yetkilendirilmemiş İstek <br>" + code + " <br> " + url,
        403: "Yasak İstek <br>" + code + " <br> " + url,
        404: "Cevap Bulunamadı <br>" + code + " <br> " + url,
        405: "Methoda İzin Verilmiyor <br>" + code + " <br> " + url,
        500: "Sunucu hatayla karşılaştı ve isteği gerçekleştiremiyor. <br>" + code + " <br> " + url,
        501: "Sunucu Methodu Tanımıyor <br>" + code + " <br> " + url,
        502: "Bozuk Ağ Geçidi <br>" + code + " <br> " + url,
        503: "Hizmet Kullanılmıyor <br>" + code + " <br> " + url,
        504: "Zaman Aşımı <br>" + code + " <br> " + url,
        505: "HTTP Protokolü Desteklenmiyor <br>" + code + " <br> " + url
      };
      return error[code];
    };
    errorHandler = function(deferred, msg, code, headers, config) {
      var err;
      deferred.reject(msg);
      err = errorCode(code, config.url);
      $ionicPopup.alert({
        title: 'İstek Hatası',
        template: err
      });
      return $ionicLoading.hide();
    };
    postDeferred = function(url, data) {
      var deferred;
      $ionicLoading.show({
        template: '<span ng-click="refresh()">Yükleniyor...</span>'
      });
      deferred = $q.defer();
      $http({
        url: url,
        data: data,
        method: 'POST'
      }).success(function(response) {
        $ionicLoading.hide();
        return deferred.resolve(response);
      }).error(function(msg, code, headers, config) {
        return errorHandler(deferred, msg, code, headers, config);
      });
      return deferred.promise;
    };
    getDeferred = function(url) {
      var deferred;
      $ionicLoading.show({
        template: 'Yükleniyor...'
      });
      deferred = $q.defer();
      $http({
        url: url
      }).success(function(response) {
        $ionicLoading.hide();
        return deferred.resolve(response);
      }).error(function(msg, code, headers, config) {
        return errorHandler(deferred, msg, code, headers, config);
      });
      return deferred.promise;
    };
    return {
      login: function(data) {
        return postDeferred(Constants.urls.login, data);
      },
      api: function(data) {
        return getDeferred(Constants.urls.api);
      }
    };
  }

  return Remote;

})();

angular.module('app').factory('Constants', ['$state', '$ionicPopup', Constants]).factory('Remote', ['$rootScope', '$http', '$q', '$log', '$timeout', '$ionicPopup', '$ionicLoading', 'Constants', Remote]);

var Routes;

Routes = (function() {
  function Routes($stateProvider, $urlRouterProvider, $ionicAppProvider) {
    var settings;
    $ionicAppProvider.identify({
      app_id: 'a4f2e8f2',
      api_key: '719d34643850915178cf9af25f79d0dba20daecb17194d4f'
    });
    $stateProvider.state("app", {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: "mainController"
    }).state("app.categories", {
      url: "/categories",
      views: {
        menuContent: {
          templateUrl: "templates/categories.html",
          controller: 'categoriesController'
        }
      }
    }).state("app.categoryDetail", {
      url: "/categories/:id",
      views: {
        menuContent: {
          templateUrl: "templates/categoryDetail.html",
          controller: 'categoryDetailController'
        }
      }
    }).state("conf", {
      url: "/conf",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: "mainController"
    }).state("conf.settings", {
      url: "/settings",
      views: {
        menuContent: {
          templateUrl: "templates/settings.html",
          controller: "settingsController"
        }
      }
    });
    settings = JSON.parse(localStorage.getItem('settings')) || {
      username: 0
    };
    if (settings.username) {
      $urlRouterProvider.otherwise("/conf/settings");
    } else {
      $urlRouterProvider.otherwise("/app/categories");
    }
  }

  return Routes;

})();

angular.module('app').config(['$stateProvider', '$urlRouterProvider', '$ionicAppProvider', Routes]);

var AppModals, LocalDB;

AppModals = (function() {
  function AppModals(ModalService) {
    return {
      categoryItemDetail: function(params) {
        return ModalService.show('templates/modal/categoryItemDetailController.html', 'categoryItemDetailController', params);
      }
    };
  }

  return AppModals;

})();

LocalDB = (function() {
  function LocalDB() {
    return {
      get: function() {
        return console.log('qweasd');
      }
    };
  }

  return LocalDB;

})();

angular.module('app').factory('AppModals', ['ModalService', AppModals]).factory('LocalDB', [LocalDB]);


/*
Usage:

ModalService
.show('<templateUrl>', '<controllerName> or <controllerName as ..>', <parameters obj>)
.then(function(result) {
    // result
}, function(err) {
    // error
});
 */
(function() {
  var ModalService, keyboardActive, serviceId;
  ModalService = function($ionicModal, $rootScope, $q, $injector, $controller) {
    var _cleanup, _evalController, show;
    show = function(templateUrl, controller, parameters) {
      var ctrlInstance, deferred, modalScope, thisScopeId;
      deferred = $q.defer();
      ctrlInstance = void 0;
      modalScope = $rootScope.$new();
      thisScopeId = modalScope.$id;
      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: modalScope,
        animation: 'slide-in-up'
      }).then((function(modal) {
        var ctrlEval, locals;
        modalScope.modal = modal;
        modalScope.openModal = function() {
          return modalScope.modal.show();
        };
        document.addEventListener('tap', function(event) {
          var target;
          target = angular.element(event.target);
          if (target.hasClass('modalbackdrop')) {
            return $rootScope.$apply(function() {
              if (keyboardActive) {
                return cordova.plugins.Keyboard.close();
              } else {
                return $rootScope.$broadcast('modal-backdrop-click');
              }
            });
          }
        });
        modalScope.closeModal = function(result) {
          deferred.resolve(result);
          if (keyboardActive) {
            return cordova.plugins.Keyboard.close();
          } else {
            return modalScope.modal.hide();
          }
        };
        modalScope.$on('modal.hidden', function(thisModal) {
          var modalScopeId;
          if (thisModal.currentScope) {
            modalScopeId = thisModal.currentScope.$id;
            if (thisScopeId === modalScopeId) {
              deferred.resolve(null);
              return _cleanup(thisModal.currentScope);
            }
          }
        });
        locals = {
          '$scope': modalScope,
          'parameters': parameters
        };
        ctrlEval = _evalController(controller);
        ctrlInstance = $controller(controller, locals);
        if (ctrlEval.isControllerAs) {
          ctrlInstance.openModal = modalScope.openModal;
          ctrlInstance.closeModal = modalScope.closeModal;
        }
        return modalScope.modal.show();
      }), function(err) {
        return deferred.reject(err);
      });
      return deferred.promise;
    };
    _cleanup = function(scope) {
      scope.$destroy();
      if (scope.modal) {
        return scope.modal.remove();
      }
    };
    _evalController = function(ctrlName) {
      var fragments, result;
      result = {
        isControllerAs: false,
        controllerName: '',
        propName: ''
      };
      fragments = (ctrlName || '').trim().split(/\s+/);
      result.isControllerAs = fragments.length === 3 && (fragments[1] || '').toLowerCase() === 'as';
      if (result.isControllerAs) {
        result.controllerName = fragments[0];
        result.propName = fragments[2];
      } else {
        result.controllerName = ctrlName;
      }
      return result;
    };
    return {
      show: show
    };
  };
  'use strict';
  serviceId = 'ModalService';
  angular.module('app').factory(serviceId, ['$ionicModal', '$rootScope', '$q', '$injector', '$controller', ModalService]);
  keyboardActive = false;
  window.addEventListener('native.keyboardshow', function() {
    return keyboardActive = true;
  });
  return window.addEventListener('native.keyboardhide', function() {
    return keyboardActive = false;
  });
})();
