from django.conf.urls import include, url, static
from django.contrib import admin
from django.conf import settings
from apps.api import views as api_view

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^api/$', api_view.Root.as_view()),
]

urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)