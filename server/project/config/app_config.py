# coding=utf-8
from django.apps import AppConfig


class ProductsConfig(AppConfig):
    name = u"apps.product"
    verbose_name = u"Lokanta"


class SettingsConfig(AppConfig):
    name = u"apps.settings"
    verbose_name = u"Yönetim"