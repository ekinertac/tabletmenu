# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0005_auto_20150624_1916'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='description',
            field=redactor.fields.RedactorField(default=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='category',
            name='user',
            field=models.ForeignKey(editable=False, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='product',
            name='user',
            field=models.ForeignKey(editable=False, to=settings.AUTH_USER_MODEL),
        ),
    ]
