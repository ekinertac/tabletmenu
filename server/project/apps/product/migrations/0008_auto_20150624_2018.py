# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0007_auto_20150624_2012'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='days',
        ),
        migrations.AddField(
            model_name='product',
            name='days',
            field=multiselectfield.db.fields.MultiSelectField(blank=True, max_length=13, null=True, choices=[(1, 'Pazartesi'), (2, 'Sal\u0131'), (3, '\xc7ar\u015famba'), (4, 'Per\u015fembe'), (5, 'Cuma'), (6, 'Cumartesi'), (7, 'Pazar')]),
        ),
        migrations.DeleteModel(
            name='Days',
        ),
    ]
