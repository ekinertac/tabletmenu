# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0006_auto_20150624_1947'),
    ]

    operations = [
        migrations.CreateModel(
            name='Days',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='product',
            name='end_date',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='is_serviced',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='product',
            name='start_date',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='days',
            field=models.ManyToManyField(to='product.Days', null=True, blank=True),
        ),
    ]
