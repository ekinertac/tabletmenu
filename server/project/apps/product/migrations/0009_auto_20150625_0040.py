# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields
from django.conf import settings
import multiselectfield.db.fields
import apps.product.models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0008_auto_20150624_2018'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': 'Kategori', 'verbose_name_plural': 'Kategoriler'},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'verbose_name': '\xdcr\xfcn', 'verbose_name_plural': '\xdcr\xfcnler'},
        ),
        migrations.AlterModelOptions(
            name='productimage',
            options={'verbose_name': '\xdcr\xfcn G\xf6rseli', 'verbose_name_plural': '\xdcr\xfcn G\xf6rselleri'},
        ),
        migrations.AlterField(
            model_name='category',
            name='image',
            field=models.ImageField(upload_to=apps.product.models.upload_to_category, null=True, verbose_name='G\xf6rsel', blank=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Ba\u015fl\u0131k'),
        ),
        migrations.AlterField(
            model_name='category',
            name='user',
            field=models.ForeignKey(editable=False, to=settings.AUTH_USER_MODEL, verbose_name='Kullan\u0131c\u0131'),
        ),
        migrations.AlterField(
            model_name='product',
            name='category',
            field=models.ForeignKey(verbose_name='Kategori', to='product.Category'),
        ),
        migrations.AlterField(
            model_name='product',
            name='days',
            field=multiselectfield.db.fields.MultiSelectField(choices=[(1, 'Pazartesi'), (2, 'Sal\u0131'), (3, '\xc7ar\u015famba'), (4, 'Per\u015fembe'), (5, 'Cuma'), (6, 'Cumartesi'), (7, 'Pazar')], max_length=13, blank=True, help_text='\xdcr\xfcn\xfcn servis edilece\u011fi g\xfcnleri belirtiniz. Herhangi bir se\xe7im yapmazsan\u0131z \xfcr\xfcn her g\xfcn men\xfcde g\xf6r\xfcn\xfcr.', null=True, verbose_name='Servis G\xfcnleri'),
        ),
        migrations.AlterField(
            model_name='product',
            name='description',
            field=redactor.fields.RedactorField(help_text='\xdcr\xfcn\xfcn\xfcz hakk\u0131nda bir\u015feyler yaz\u0131n.', verbose_name='A\xe7\u0131klama'),
        ),
        migrations.AlterField(
            model_name='product',
            name='end_date',
            field=models.TimeField(help_text='\xdcr\xfcn\xfcn servisten kald\u0131r\u0131ld\u0131\u011f\u0131 saati belirtiniz. Herhangi bir se\xe7im yapmazsan\u0131z \xfcr\xfcn her saat men\xfcde g\xf6r\xfcn\xfcr.', null=True, verbose_name='Biti\u015f Saati', blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='is_serviced',
            field=models.BooleanField(default=True, help_text='\xdcr\xfcn servisten kald\u0131r\u0131ld\u0131ysa bu kutucu\u011fu bo\u015f b\u0131rak\u0131n\u0131z.', verbose_name='Serviste'),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.FloatField(help_text="Sonuna 'TL' Yazmay\u0131n\u0131z", verbose_name='Sat\u0131\u015f Fiyat\u0131'),
        ),
        migrations.AlterField(
            model_name='product',
            name='start_date',
            field=models.TimeField(help_text='\xdcr\xfcn\xfcn servis edilmeye ba\u015flad\u0131\u011f\u0131 saati belirtiniz. Herhangi bir se\xe7im yapmazsan\u0131z \xfcr\xfcn her saat men\xfcde g\xf6r\xfcn\xfcr.', null=True, verbose_name='Ba\u015flang\u0131\xe7 Saati', blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Ba\u015fl\u0131k'),
        ),
        migrations.AlterField(
            model_name='product',
            name='user',
            field=models.ForeignKey(editable=False, to=settings.AUTH_USER_MODEL, verbose_name='Kullan\u0131c\u0131'),
        ),
        migrations.AlterField(
            model_name='productimage',
            name='image',
            field=models.ImageField(upload_to=apps.product.models.upload_to_products, null=True, verbose_name='\xdcr\xfcn G\xf6rseli', blank=True),
        ),
    ]
