# coding=utf-8
from django.contrib import admin
from apps.product.models import Category, Product, ProductImage, DAYS_CHOICES


class CategoryAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(CategoryAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs

        return qs.filter(user=request.user)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super(CategoryAdmin, self).save_model(request, obj, form, change)


class ProductImageInline(admin.TabularInline):
    model = ProductImage


class ProductAdmin(admin.ModelAdmin):
    inlines = [ProductImageInline]
    list_display = [
        'title', 'category', 'price',
        'start_date', 'end_date', 'serve_days',
        'is_serviced'
    ]

    def get_queryset(self, request):
        qs = super(ProductAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super(ProductAdmin, self).save_model(request, obj, form, change)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "category" and not request.user.is_superuser:
            kwargs["queryset"] = Category.objects.filter(user=request.user)

        return super(ProductAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def serve_days(self, obj):
        return obj.get_days_display()
    serve_days.short_description = u"Servis Günleri"

admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)

