# coding=utf-8
import os

from django.contrib.auth.models import User
from django.db import models
from multiselectfield import MultiSelectField
from redactor.fields import RedactorField


def upload_to_products(instance, filename):
    return os.path.join(
        "%s" % instance.parent.category.user.username,
        "product",
        filename
    )


def upload_to_category(instance, filename):
    return os.path.join(
        "%s" % instance.user.username,
        "category",
        filename
    )


DAYS_CHOICES = (
    (1, u'Pazartesi'),
    (2, u'Salı'),
    (3, u'Çarşamba'),
    (4, u'Perşembe'),
    (5, u'Cuma'),
    (6, u'Cumartesi'),
    (7, u'Pazar'),
)


class Category(models.Model):
    user = models.ForeignKey(User, editable=False, verbose_name=u"Kullanıcı")
    title = models.CharField(max_length=255, verbose_name=u"Başlık")
    image = models.ImageField(upload_to=upload_to_category, blank=True, null=True, verbose_name=u"Görsel")

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u"Kategori"
        verbose_name_plural = u"Kategoriler"


class Product(models.Model):
    user = models.ForeignKey(User, editable=False, verbose_name=u"Kullanıcı")
    category = models.ForeignKey(Category, verbose_name=u"Kategori")
    title = models.CharField(max_length=255, verbose_name=u"Başlık")
    price = models.FloatField(verbose_name=u"Satış Fiyatı", help_text=u"Sonuna 'TL' Yazmayınız")

    description = RedactorField(
        verbose_name=u"Açıklama",
        help_text=u"Ürününüz hakkında birşeyler yazın."
    )

    days = MultiSelectField(
        choices=DAYS_CHOICES,
        blank=True,
        null=True,
        verbose_name=u"Servis Günleri",
        help_text=u"Ürünün servis edileceği günleri belirtiniz. "
                  u"Herhangi bir seçim yapmazsanız ürün her gün menüde görünür."
    )

    start_date = models.TimeField(
        blank=True,
        null=True,
        verbose_name=u"Başlangıç Saati",
        help_text=u"Ürünün servis edilmeye başladığı saati belirtiniz. "
                  u"Herhangi bir seçim yapmazsanız ürün her saat menüde görünür."
    )

    end_date = models.TimeField(
        blank=True,
        null=True,
        verbose_name=u"Bitiş Saati",
        help_text=u"Ürünün servisten kaldırıldığı saati belirtiniz. "
                  u"Herhangi bir seçim yapmazsanız ürün her saat menüde görünür."
    )

    is_serviced = models.BooleanField(
        default=True,
        verbose_name=u"Serviste",
        help_text=u"Ürün servisten kaldırıldıysa bu kutucuğu boş bırakınız."
    )

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u"Ürün"
        verbose_name_plural = u"Ürünler"


class ProductImage(models.Model):
    parent = models.ForeignKey(Product)
    image = models.ImageField(upload_to=upload_to_products, blank=True, null=True, verbose_name=u"Ürün Görseli")

    class Meta:
        verbose_name = u"Ürün Görseli"
        verbose_name_plural = u"Ürün Görselleri"
