from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import Design


admin.site.register(Design, SingletonModelAdmin)

