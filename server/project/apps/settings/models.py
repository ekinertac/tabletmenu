# coding=utf-8
from django.db import models
from solo.models import SingletonModel


class Design(SingletonModel):
    css = models.TextField(blank=True, null=True)
    script = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = u"Tasarım"