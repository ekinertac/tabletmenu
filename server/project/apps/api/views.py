from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.core import serializers
from django.views.generic import View
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from apps.api.serializer import CategorySerializer, ProductListSerializer
from apps.product.models import Category, Product


class Root(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get_queryset(self, *args, **kwargs):
        user = self.request.GET.get('user')
        qs = super(Root, self).get_queryset()
        result = qs.filter(user__username=user) if user else Category.objects.none()
        return result


class Product(generics.RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer
    lookup_url_kwarg = 'pk'