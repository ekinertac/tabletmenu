import json
from django import http
from django.conf import settings
from django.core import serializers
import redis


class JSONResponseMixin(object):
    """
    models.py

    class TextModel(models.Model):
        user = models.ForeignKey(User)
        country = models.CharField()
        city = models.CharField()


    views.py;

    from utils import JSONResponseMixin, filter

    class TestModel(JSONResponseMixin, BaseDetailView):
        exclude ['city']

        def prepare_country(self, value):
            return value.title()

        def get(self, request, *args, **kwargs):
            qs = TestModel.objects.filter(**filter(request.GET, {'user': int}))
            return self.render_to_response(qs)

    url = filter querysting works
    example ; /test/?user=<id>
    """

    exclude = None

    def render_to_response(self, qs):
        return self.get_json_response(qs)

    def _exclude(self, json_data):
        if self.exclude:
            for data in json_data:
                for ex in self.exclude:
                    del data['fields'][ex]
        return json_data

    def data_dumps(self, data):
        return json.dumps(data)

    def _prepare(self, json_data):
        for data in json_data:
            for field in data['fields']:
                if hasattr(self, "prepare_%s" % field):
                    data['fields'][field] = getattr(self, "prepare_%s" % field)(data['fields'][field])
        return json_data

    def get_json_response(self, qs, **httpresponse_kwargs):
        data = self.convert_context_to_json(qs)
        data = self._exclude(data)
        data = self._prepare(data)
        data = self.data_dumps(data)
        return http.HttpResponse(data, content_type="application/json")

    def convert_context_to_json(self, qs):
        return json.loads(serializers.serialize('json', qs))


class RedisConnection(object):
    def __init__(self, **kwargs):
        self.host = settings.REDIS_HOST
        self.port = settings.REDIS_PORT
        self.db = settings.REDIS_DATABASES[kwargs['db']]
        self.connection = self._connection()

    def _connection(self):
        return redis.StrictRedis(host=self.host, port=self.port, db=self.db)

    def push(self, key, value):
        return self.connection.set(key, value)

    def pushex(self, key, value):
        return self.connection.setex(key, settings.REDIS_KEY_EXPIRE_TIME, value)

    def pull(self, key):
        return self.connection.get(key)

    def delete(self, key):
        return self.connection.delete(key)

    def rename(self, key, newkey):
        return self.connection.rename(key, newkey)