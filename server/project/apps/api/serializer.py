# coding=utf-8
from rest_framework import serializers
from apps.product.models import Category, Product, ProductImage


class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage


class ProductListSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()

    class Meta:
        model = Product

    def get_images(self, obj):
        image_list = [u"%s" % i.image for i in obj.productimage_set.all()]
        return image_list


class CategorySerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField()

    class Meta:
        model = Category

    def get_products(self, obj):
        products = Product.objects.filter(category=obj)
        serialized = ProductListSerializer(instance=products, many=True)
        return serialized.data