from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from apps.api import views


urlpatterns = patterns('apps.api.views',
    url(r'^$',                              views.RootView.as_view(),               name='root'),
    url(r'^category/$',                     views.CategoryView.as_view(),           name='category'),
    url(r'^category/(?P<pk>[0-9]+)/$',      views.CategoryDetailView.as_view(),     name='category_detail'),
)

urlpatterns = format_suffix_patterns(urlpatterns)