class Configuration extends Run
    constructor: (
        $rootScope,
        $ionicPlatform, $ionicLoading
    ) ->

        if window.cordova and window.cordova.plugins.Keyboard
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar true

        StatusBar.styleDefault() if window.StatusBar

        # Ionic Deploy
        # $rootScope.deployStatus = "Uygulama Güncelleniyor..."
        # $ionicDeploy.watch().then (->), (->), (hasUpdate) ->
        #     if hasUpdate
        #         $ionicLoading.show
        #             template: $rootScope.deployStatus

        #         $ionicDeploy.update().then ((res) ->
        #             $rootScope.deployStatus = "Uygulama Güncellendi. Lütfen Bekleyiniz..."
        #         ), ((err) ->
        #             $rootScope.deployStatus = "Uygulama güncellenirken bir hata oluştu. Hata: #{err}"
        #         ), ((prog) ->
        #             $rootScope.deployStatus = "Uygulama Güncelleniyor... #{prog}"
        #         )