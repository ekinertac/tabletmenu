class Routes extends Config
    constructor: ($stateProvider, $urlRouterProvider, $ionicAppProvider)->
        $ionicAppProvider.identify
            app_id: 'a4f2e8f2',
            api_key: '719d34643850915178cf9af25f79d0dba20daecb17194d4f'

        $stateProvider
        .state "app",
            url: "/app"
            abstract: true
            templateUrl: "templates/menu.html"
            controller: "mainController"

        .state "app.categories",
            url: "/categories"
            views:
                menuContent:
                    templateUrl: "templates/categories.html"
                    controller: 'categoriesController'

        .state "app.categoryDetail",
            url: "/categories/:id"
            views:
                menuContent:
                    templateUrl: "templates/categoryDetail.html"
                    controller: 'categoryDetailController'

        .state "conf",
            url: "/conf"
            abstract: true
            templateUrl: "templates/menu.html"
            controller: "mainController"

        .state "conf.settings",
            url: "/settings"
            views:
                menuContent:
                    templateUrl: "templates/settings.html"
                    controller: "settingsController"

        settings = JSON.parse(localStorage.getItem('settings')) || {username: 0}

        if settings.username
            $urlRouterProvider.otherwise "/conf/settings"
        else
            $urlRouterProvider.otherwise "/app/categories"
