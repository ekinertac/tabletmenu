# Ionic modal service from http://forum.ionicframework.com/t/ionic-modal-service-with-extras/15357

###
Usage:

ModalService
.show('<templateUrl>', '<controllerName> or <controllerName as ..>', <parameters obj>)
.then(function(result) {
    // result
}, function(err) {
    // error
});
###
do ->
    ModalService = ($ionicModal, $rootScope, $q, $injector, $controller) ->
        show = (templateUrl, controller, parameters) ->
            # Grab the injector and create a new scope
            deferred = $q.defer()
            ctrlInstance = undefined
            modalScope = $rootScope.$new()
            thisScopeId = modalScope.$id
            $ionicModal.fromTemplateUrl(templateUrl,
                scope: modalScope
                animation: 'slide-in-up').then ((modal) ->
                modalScope.modal = modal

                modalScope.openModal = ->
                    modalScope.modal.show()

                document.addEventListener 'tap', (event) ->
                    target = angular.element(event.target)
                    if target.hasClass('modalbackdrop')
                        $rootScope.$apply ->
                            if keyboardActive
                                cordova.plugins.Keyboard.close()
                            else
                                $rootScope.$broadcast 'modal-backdrop-click'

                modalScope.closeModal = (result) ->
                    deferred.resolve result
                    if keyboardActive
                        cordova.plugins.Keyboard.close()
                    else
                        modalScope.modal.hide()

                modalScope.$on 'modal.hidden', (thisModal) ->
                    if thisModal.currentScope
                        modalScopeId = thisModal.currentScope.$id
                        if thisScopeId == modalScopeId
                            deferred.resolve null
                            _cleanup thisModal.currentScope

                # Invoke the controller
                locals =
                    '$scope': modalScope
                    'parameters': parameters
                ctrlEval = _evalController(controller)
                ctrlInstance = $controller(controller, locals)
                if ctrlEval.isControllerAs
                    ctrlInstance.openModal = modalScope.openModal
                    ctrlInstance.closeModal = modalScope.closeModal
                modalScope.modal.show()

            ), (err) ->
                deferred.reject err
            deferred.promise

        _cleanup = (scope) ->
            scope.$destroy()
            if scope.modal
                scope.modal.remove()


        _evalController = (ctrlName) ->
            result =
                isControllerAs: false
                controllerName: ''
                propName: ''
            fragments = (ctrlName or '').trim().split(/\s+/)
            result.isControllerAs = fragments.length == 3 and (fragments[1] or '').toLowerCase() == 'as'
            if result.isControllerAs
                result.controllerName = fragments[0]
                result.propName = fragments[2]
            else
                result.controllerName = ctrlName
            result

        { show: show }

    'use strict'
    serviceId = 'ModalService'
    angular.module('app').factory serviceId, [
        '$ionicModal'
        '$rootScope'
        '$q'
        '$injector'
        '$controller'
        ModalService
    ]
    keyboardActive = false
    window.addEventListener 'native.keyboardshow', ->
        keyboardActive = true

    window.addEventListener 'native.keyboardhide', ->
        keyboardActive = false