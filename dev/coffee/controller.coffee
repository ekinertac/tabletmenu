class Main extends Controller
    constructor: ($scope, $ionicModal, $timeout, $state) ->
        $scope.menuItems = [{
            text: "Categories"
            action: () ->
                $state.go('app.categories')
        },{
            text: "Call Waiter"
            action: () ->
                console.log "Call Waiter"
        }, {
            text: "Request Check",
            action: () ->
                console.log "Request Check"
        }, {
            text: "About DabbletMenu",
            action: () ->
                console.log "About DabbletMenu"
        }, {
            text: "Settings",
            action: () ->
                $state.go('conf.settings')
        }]


class Settings extends Controller
    constructor: ($scope, $ionicPopup, LocalDB) ->
        $scope.viewTitle = "Settings"
        $scope.viewId = "settings-view"

        $scope.settings = JSON.parse(localStorage.getItem('settings'))

        $scope.save = ->
            $ionicPopup.confirm
                title: 'Kaydet'
                template: 'Ayarları değiştirmek istediğinizden emin misiniz ?'
                cancelText: 'Hayır'
                okText: 'Evet'
            .then (res) ->
                if res
                    localStorage.setItem('settings', JSON.stringify($scope.settings))


class Categories extends Controller
    constructor: ($scope, $state) ->
        $scope.viewTitle = "Categories"
        $scope.viewId = "categoryMain"

        $scope.categories = [
            {
                title: "Appetizers",
                id: 1,
                image: "img/thumb/appetizers.jpg",
                like: 21
            }, {
                title: "Salads",
                id: 2,
                image: "img/thumb/Chopped-Salad_550x310.jpg",
                like: 43
            }, {
                title: "Meats",
                id: 3,
                image: "img/thumb/5491610d-3014-41e4-a4b1-72e1c0a8641a.jpg",
                like: 23
            }, {
                title: "Seafood",
                id: 4,
                image: "img/thumb/healthy-seafood.jpg",
                like: 25
            }, {
                title: "Fish",
                id: 5,
                image: "img/thumb/gida02.jpg",
                like: 54
            }, {
                title: "Chef Special",
                id: 6,
                image: "img/thumb/da-tandoor-victoria-01.jpg",
                like: 74
            }, {
                title: "Side Orders",
                id: 7,
                image: "img/thumb/4371111_ec17e07834.jpg",
                like: 61
            }, {
                title: "Drinks",
                id: 8,
                image: "img/thumb/ProductsSoftDrinks-604-st.jpg",
                like: 22
            }
        ]

        $scope.goCategoryDetail = (id) ->
            $state.go('app.categoryDetail', {id: id})

class CategoryDetail extends Controller
    constructor: ($scope, AppModals) ->
        $scope.category = {
            id: 1,
            title: "Appetizers",
            image: "img/thumb/appetizers.jpg",
            isImageless: false,
            products: [{
                title: 'Chicken, Beef Or Cheese Mini Patties',
                price: "3.75",
                image: 'img/thumb/28358_l.jpg'
            },{
                title: 'Fried Cassava',
                price: "3.75",
                desc: "With green peppers, red peppers and onion",
                image: 'img/thumb/YucaFritamini.jpg'
            },{
                title: 'Sampler Platter (Small)',
                price: "12"
                desc: "With combination of fried meat, green plantains and cassava, with fried cheese add $2.00",
                image: 'img/thumb/appetizer-platter.jpg'
            },{
                title: 'Grilled Steak',
                price: "10.50",
                desc: 'Sautee with green peppers, red peppers and onion',
                image: 'img/thumb/250514100857-beef-steak.jpg'
            },{
                title: 'Seafood Combination',
                price: "30.50",
                desc: 'With lobster, shrimps, calamari, octopus & mussels',
                image: 'img/thumb/55011.jpg'
            },{
                title: 'Macaroni Shells',
                price: "30.50",
                desc: 'With lobster, shrimps, calamari, octopus & mussels',
                image: 'img/thumb/tumblr_mzaovujDaf1qg5ngpo1_500.jpg'
            },{
                title: 'Macaroni Shells',
                price: "30.50",
                desc: 'With lobster, shrimps, calamari, octopus & mussels',
                image: 'img/thumb/250514100857-beef-steak.jpg'
            },{
                title: 'Grilled Chicken Salad',
                price: "2500",
                image: 'img/thumb/appetizer-platter.jpg'
            }]
        }

        $scope.openDetail = (id) ->
            AppModals.categoryItemDetail({id: id})


class CategoryItemDetail extends Controller
    constructor: ($scope, AppModals, parameters) ->
        $scope.item = {
            title: 'Macaroni Shells',
            price: "30.50",
            desc: 'With lobster, shrimps, calamari, octopus & mussels',
            longDesc: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going yo use a passage of Lorem Ipsum."
            images: [
                'img/thumb/tumblr_mzaovujDaf1qg5ngpo1_500.jpg',
                'img/thumb/250514100857-beef-steak.jpg',
                'img/thumb/appetizer-platter.jpg',
                'img/thumb/55011.jpg'
            ]
        }
