class Constants extends Factory
    constructor: ($state, $ionicPopup) ->
        settings = JSON.parse(localStorage.getItem('settings'))
        apiRoot = "127.0.0.1:8000"

        unless settings
            return $ionicPopup.alert
                title: 'Server bilgisi girilmemiş'
                template: 'Server bilgisi girebilmek için ayarlar bölümüne yönlendirileceksiniz.'
                okText: 'Tamam'
            .then ->
                $state.go('conf.settings')

        return {
            urls:{
                login:  "http://#{apiRoot}/login"
                api:    "http://#{apiRoot}/api?user=#{settings.username}"
            }
        }

class Remote extends Factory
    constructor: ($rootScope, $http, $q, $log, $timeout, $ionicPopup, $ionicLoading, Constants)->
        errorCode = (code, url) ->
            error =
                0   : "Bağlantınızda sorunlar bulunmaktadır. Lütfen daha sonra tekrar deneyiniz."
                400 : "Hatalı İstek <br>#{code} <br> #{url}"
                401 : "Yetkilendirilmemiş İstek <br>#{code} <br> #{url}"
                403 : "Yasak İstek <br>#{code} <br> #{url}"
                404 : "Cevap Bulunamadı <br>#{code} <br> #{url}"
                405 : "Methoda İzin Verilmiyor <br>#{code} <br> #{url}"
                500 : "Sunucu hatayla karşılaştı ve isteği gerçekleştiremiyor. <br>#{code} <br> #{url}"
                501 : "Sunucu Methodu Tanımıyor <br>#{code} <br> #{url}"
                502 : "Bozuk Ağ Geçidi <br>#{code} <br> #{url}"
                503 : "Hizmet Kullanılmıyor <br>#{code} <br> #{url}"
                504 : "Zaman Aşımı <br>#{code} <br> #{url}"
                505 : "HTTP Protokolü Desteklenmiyor <br>#{code} <br> #{url}"

            return error[code]

        errorHandler = (deferred, msg, code, headers, config) ->
            deferred.reject(msg)
            err = errorCode(code, config.url)

            $ionicPopup.alert
                title: 'İstek Hatası',
                template: err

            $ionicLoading.hide()

        postDeferred = (url, data) ->
            $ionicLoading.show
                template: '<span ng-click="refresh()">Yükleniyor...</span>'

            deferred = $q.defer()
            $http
                url: url
                data: data
                method: 'POST'
            .success (response) ->
                $ionicLoading.hide()

                deferred.resolve(response)
            .error (msg, code, headers, config) ->
                errorHandler(deferred, msg, code, headers, config)
            return deferred.promise

        getDeferred = (url) ->
            $ionicLoading.show({template: 'Yükleniyor...'})
            deferred = $q.defer()
            $http
                url: url
            .success (response) ->
                $ionicLoading.hide()

                deferred.resolve(response)
            .error (msg, code, headers, config) ->
                errorHandler(deferred, msg, code, headers, config)
            return deferred.promise

        return {
            login: (data) ->
                return postDeferred(Constants.urls.login, data)
            api: (data) ->
                return getDeferred(Constants.urls.api)
        }
