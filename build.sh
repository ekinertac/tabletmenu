date1=$(date +"%s");

# gulp build;


ionic build --release android;

mkdir -p platforms/android/keystore;
mkdir -p adhoc;

rm adhoc/TabletMenu.apk;

if [ ! -f platforms/android/keystore/android-key.keystore ]; then
    keytool \
        -genkey -v \
        -keystore platforms/android/keystore/android-key.keystore \
        -alias alias_name \
        -keyalg RSA \
        -keysize 2048 \
        -validity 10000;
fi

jarsigner -verbose \
    -sigalg SHA1withRSA \
    -digestalg SHA1 \
    -keystore platforms/android/keystore/android-key.keystore \
    /Users/ekinertac/Apps/ionic/tabletmenu/platforms/android/build/outputs/apk/android-release-unsigned.apk \
    alias_name;

zipalign \
    -v 4 \
    /Users/ekinertac/Apps/ionic/tabletmenu/platforms/android/build/outputs/apk/android-release-unsigned.apk \
    adhoc/TabletMenu.apk;

date2=$(date +"%s");
diff=$(($date2-$date1));
echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed.";

# ftp -n server.net.tr <<END_SCRIPT
# quote USER ekin
# quote PASS 13481986
# put adhoc/Dabblet.apk Dabblet.apk
# quit
# END_SCRIPT
# exit 0